﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicNote : MonoBehaviour
{
    [SerializeField] float m_FloatTime = 2f;
    [SerializeField] float m_RisingSpeed = 100f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyGO());
    }

    private IEnumerator DestroyGO()
    {
        yield return new WaitForSeconds(m_FloatTime);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        var newPos = new Vector3(transform.position.x, transform.position.y + m_RisingSpeed * Time.deltaTime, transform.position.z);
        transform.position = newPos;
    }

    
}
