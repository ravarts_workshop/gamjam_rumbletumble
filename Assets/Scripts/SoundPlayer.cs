﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{

    private AudioSource m_AudioSource = null;
    // Start is called before the first frame update
    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    /// <summary>
    /// Plays requested Clip
    /// </summary>
    /// <param name="clip"></param>
    public void PlaySound(AudioClip clip)
    {
        if (!clip) { return; }
        m_AudioSource.PlayOneShot(clip);
    }
    /// <summary>
    /// Plays requested Clip with volume
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="volume"></param>
   
    public void PlaySound(AudioClip clip, float volume)
    {
        if (!clip) { return; }
        m_AudioSource.PlayOneShot(clip, volume);
    }

}
