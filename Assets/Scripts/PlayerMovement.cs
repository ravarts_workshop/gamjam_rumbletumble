﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5f;
    //for Jump or other moves
    private float oldSpeed;
    public float scale = 1f;
    private float rayLength = 1f;
    public int jumpLength = 3;
    private bool canMove;

    private float delay;
    //Animator
    private Animator animator;

    //GotHit
    public bool hitted;

    //Rotation
    Vector3 up = Vector3.zero;
    Vector3 right = new Vector3(0, 90, 0);
    Vector3 down = new Vector3(0, 180, 0);
    Vector3 left = new Vector3(0, 270, 0);
    Vector3 currentDirection = Vector3.zero;

    Vector3 nextPos, destination, direction;

    //new Location after getHit
    public Vector3 hitLocation;

    //Player
    private Player m_Player = null;
    void Start()
    {
        currentDirection = up;
        nextPos = Vector3.forward;
        destination = transform.position;
        oldSpeed = speed;
        hitLocation = transform.position;

        animator = GetComponentInChildren<Animator>();
        m_Player = GetComponent<Player>();
        delay = FindObjectOfType<SongManager>().SecPerBeat / 2;
    }

    void Update()
    {
        Move();
    }
    /// <summary>
    /// 
    /// </summary>
    internal void ExecuteMoveInput(PlayerAction action)
    {
        switch (action)
        {
            case (PlayerAction.Up):
            {
                nextPos = Vector3.forward * scale;
                currentDirection = up;
                speed = oldSpeed;
                canMove = true;
                break;
            }
            case (PlayerAction.Down):
            {
                nextPos = Vector3.back * scale;
                currentDirection = down;
                speed = oldSpeed;
                canMove = true;
                break;
            }
            case (PlayerAction.Left):
            {
                nextPos = Vector3.left * scale;
                currentDirection = left;
                speed = oldSpeed;
                canMove = true;
                break;
            }
            case (PlayerAction.Right):
            {
                nextPos = Vector3.right * scale;
                currentDirection = right;
                speed = oldSpeed;
                canMove = true;
                break;
            }
            default:{
                Debug.LogWarning("undefined playerAction");
                break;
            }
        }
    }

    private void Move()
    {
        if (hitted)
        {
            destination = hitLocation;
            hitted = false;
        }

        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);


        //KNOWN BUG: Two Players walk in the sameTime in the same position 
        //Detect before that and get double Hit and return to old positions?



        //completed one step
        if (Vector3.Distance(destination, transform.position) <= 0.001f)
        {
            transform.localEulerAngles = currentDirection;
            if (canMove)
            {
                //For Walls
                if (Valid())
                {
                    if (CheckPlayer())
                    {
                        //Debug.Log("PlayerHit");
                        //Trigger Normal HitAnimation
                    }
                    else
                    {
                        destination = transform.position + nextPos;
                        direction = nextPos;
                        animator.SetTrigger("Step");
                        m_Player.PlayStepSound();
                    }
                    
                    canMove = false;
                }
                //JumpMove with Ropes
                if (CheckJump())
                {
                    //Unhitable while performing?
                    //Trigger Animation
                    currentDirection = ReverseDirection(currentDirection);
                    transform.localEulerAngles = currentDirection;
                    animator.SetTrigger("Rope");
                    m_Player.PlayRopeSound();
                    //ToDo Skip one turn
                    //Do Collision Player DMG/Sound
                    RaycastHit hit;
                    RaycastHit[] hits = Physics.RaycastAll(transform.position, -nextPos, jumpLength);
                    destination = transform.position - nextPos * jumpLength;
                    direction = -nextPos;
                    oldSpeed = speed;
                    speed *= 3;
                    canMove = false;
                    
                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i].collider.CompareTag("Player"))
                        {
                            RegisterHit();
                            //endPosition
                            if (Vector3.Distance(transform.position, hits[i].collider.transform.position) == 3 * scale)
                            {

                                Ray myRay = new Ray(destination + new Vector3(0, 0.25f, 0), transform.forward);


                                if (Physics.Raycast(myRay, out hit,rayLength * scale ))
                                {
                                    if (hit.collider.CompareTag("Player"))
                                    {
                                        //hittet Player will not Move
                                        hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position - nextPos;
                                        hit.collider.GetComponent<PlayerMovement>().GotHit();
                                        //if another one is in Line
                                        myRay = new Ray(destination + nextPos + new Vector3(0, 0.25f, 0), transform.forward);
                                        if (Physics.Raycast(myRay, out hit, rayLength * scale))
                                        {
                                            if (hit.collider.CompareTag("Player"))
                                            {
                                                RegisterHit();
                                                hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position - nextPos;
                                                hit.collider.GetComponent<PlayerMovement>().GotHit();
                                                //Move3
                                                myRay = new Ray(hit.collider.transform.position - nextPos + new Vector3(0, 0.25f, 0), transform.forward);
                                                if (Physics.Raycast(myRay, out hit, rayLength * scale))
                                                {
                                                    if (hit.collider.CompareTag("Player"))
                                                    {
                                                        RegisterHit();
                                                        hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position - nextPos;
                                                        hit.collider.GetComponent<PlayerMovement>().GotHit();
                                                    }
                                                }
                                            }
                                        }
                                        return;
                                    }
                                }
                                
                                hits[i].collider.GetComponent<PlayerMovement>().hitLocation = hits[i].collider.transform.position - nextPos;
                                hits[i].collider.GetComponent<PlayerMovement>().GotHit();
                                RegisterHit();
                                
                            }
                        }
                    }
                }
            }
        }
        //Bools
        bool Valid()
        {
            Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
            RaycastHit hit;

            Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

            if (Physics.Raycast(myRay, out hit, rayLength * scale))
            {
                if (hit.collider.CompareTag("Wall") || hit.collider.CompareTag("Rope"))
                {
                    return false;
                }
            }
            return true;
        }
        bool CheckJump()
        {
            Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
            RaycastHit hit;

            Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

            if (Physics.Raycast(myRay, out hit, rayLength * scale))
            {
                if (hit.collider.CompareTag("Rope"))
                {
                    return true;
                }
            }
            return false;
        }
        
    }
    bool CheckPlayer()
    {
        Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(myRay, out hit, rayLength * scale))
        {
            if (hit.collider.CompareTag("Player"))
            {
                animator.SetTrigger("Hit");
                //SongManager.afterBeat += PlayerCollision;
                StartCoroutine(HitDelay(hit.collider.gameObject));
                //PlayerCollision(hit.collider.gameObject);
                //hit.collider.GetComponent<PlayerMovement>().GotHit(this.transform);
                return true;
            }
        }
        return false;
    }
    IEnumerator HitDelay(GameObject hitPlayer)
    {
        yield return new WaitForSeconds( delay);
        PlayerCollision(hitPlayer);
    }
    public void PlayerCollision(GameObject hitPlayer)
    {
        // ToDo CheckLocation
        RaycastHit hit;
        hitPlayer.transform.eulerAngles = currentDirection;
        //hitPlayer.GetComponent<PlayerMovement>().currentDirection = currentDirection;
        Ray myRay = new Ray(hitPlayer.transform.position + new Vector3(0, 0.25f, 0), hitPlayer.transform.forward);

        if (Physics.Raycast(myRay, out hit, rayLength * scale))
        {
            if (hit.collider.CompareTag("Player"))
            {
                //hittet Player will not Move
                Debug.Log("DoublePlayer");
                //hitPlayer.GetComponent<PlayerMovement>().hitted = true;
                hit.collider.GetComponent<PlayerMovement>().GotHit();
                hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position;
                RegisterHit(); //Add another Point to attacking Player

                //Check if backmove is Possible
                Ray m_Ray = new Ray(transform.position + new Vector3(0, 0.25f, 0),- transform.forward);
                //push attacking Player back to not get Stun_locked?
                destination = transform.position - nextPos;

                if (Physics.Raycast(m_Ray,out hit, rayLength * scale))
                {
                    if(hit.collider.CompareTag("Player") || hit.collider.CompareTag("Rope"))
                    {
                        Debug.Log("PlayerBehind");
                        //Override destination
                        destination = transform.position;
                    }
                }
                

            }
            else if (hit.collider.CompareTag("Rope"))
            {
                PlayerMovement hittetPlayer = hitPlayer.GetComponent<PlayerMovement>();
                hittetPlayer.oldSpeed = speed;
                hittetPlayer.speed *= 2;
                //hit -> rope -> turnBack -> hit again -> hittet behind Player?
                Debug.Log("Rope");
                hittetPlayer.hitted = true;
                //WallPosition
                hittetPlayer.hitLocation = hittetPlayer.transform.position + nextPos;
                //irelevant
                hittetPlayer.transform.localEulerAngles = -currentDirection;

                //RayCast to new Position
                RaycastHit[] hits = Physics.RaycastAll(hittetPlayer.transform.position, -nextPos, jumpLength);
                hittetPlayer.hitLocation = hittetPlayer.transform.position - nextPos * jumpLength; //Maybe 4 for protection?
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].collider.CompareTag("Player") && hits[i].collider.GetComponent<PlayerMovement>() != this)
                    {
                        RegisterHit();
                        //endPosition
                        if (Vector3.Distance(hittetPlayer.transform.position, hits[i].collider.transform.position) == jumpLength)
                        {
                            Debug.Log("EndPointCollision");

                            myRay = new Ray(hittetPlayer.hitLocation + new Vector3(0, 0.25f, 0),  - transform.forward);


                            if (Physics.Raycast(myRay, out hit, rayLength * scale))
                            {
                                if (hit.collider.CompareTag("Player"))
                                {
                                    //hittet Player will not Move
                                    Debug.Log("DoublePlayer");
                                    hitPlayer.GetComponent<PlayerMovement>().GotHit();
                                    hit.collider.GetComponent<PlayerMovement>().GotHit();
                                    hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position - nextPos;
                                    RegisterHit(); //Add another Point to attacking Player

                                    myRay = new Ray(hit.collider.GetComponent<PlayerMovement>().hitLocation + new Vector3(0, 0.25f, 0), -transform.forward);
                                    if (Physics.Raycast(myRay, out hit, rayLength * scale))
                                    {
                                        if (hit.collider.CompareTag("Player"))
                                        {
                                            RegisterHit();
                                            hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position - nextPos;
                                            hit.collider.GetComponent<PlayerMovement>().GotHit();
                                            //Move3
                                            myRay = new Ray(hit.collider.transform.position - nextPos + new Vector3(0, 0.25f, 0), transform.forward);
                                            if (Physics.Raycast(myRay, out hit, rayLength * scale))
                                            {
                                                if (hit.collider.CompareTag("Player"))
                                                {
                                                    RegisterHit();
                                                    hit.collider.GetComponent<PlayerMovement>().hitLocation = hit.collider.transform.position - nextPos;
                                                    hit.collider.GetComponent<PlayerMovement>().GotHit();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            hits[i].collider.GetComponent<PlayerMovement>().hitLocation = hits[i].collider.GetComponent<PlayerMovement>().transform.position - nextPos;
                            hits[i].collider.GetComponent<PlayerMovement>().GotHit();
                            RegisterHit();

                        }
                    }
                }
                


                //AddSound();
                //Hits after this?
            }
            RegisterHit();
            return;
            
        }
        //hitPlayer.transform.position = hitPlayer.transform.position + nextPos;
        hitPlayer.transform.eulerAngles = hitPlayer.GetComponent<PlayerMovement>().currentDirection;
        hitPlayer.GetComponent<PlayerMovement>().hitLocation = hitPlayer.transform.position + nextPos;
        hitPlayer.GetComponent<PlayerMovement>().GotHit();
        RegisterHit();
    }

    //Point for Hitting //public for Calling through Animation?
    private void RegisterHit()
    {
        Debug.Log("hit");
        //TODO Add sound to List (Points) 
        if(!m_Player){ return; }

        m_Player.RegisterHit();
    }

    //HitEvents
    public void GotHit()
    {
        //Debug.Log("HITTET");
        hitted = true;
        animator.SetTrigger("GotHit");
        
    } 
    private Vector3 ReverseDirection(Vector3 oldDirection)
    {
        if (oldDirection == left)
            return right;
        else if (oldDirection == right)
            return left;
        else if (oldDirection == up)
            return down;
        else if (oldDirection == down)
            return up;
        else
            Debug.Log("wrongInput");
        return Vector3.zero;
    }
    private void AfterHitReaction(PlayerMovement pl1, PlayerMovement pl2)
    {
        if (pl1.transform.position.x > pl2.transform.position.x)
        {
            // pl1 is right 
            pl1.AddDestination(Vector3.right);
        }
        else if (pl1.transform.position.x < pl2.transform.position.x)
        {
            //left
            pl1.AddDestination(Vector3.left);
        }
        else if (pl1.transform.position.z > pl2.transform.position.z)
        {
            //top
            pl1.AddDestination(Vector3.up);
        }
        else if (pl1.transform.position.z < pl2.transform.position.z)
        {
            //below
            pl1.AddDestination(Vector3.down);
        }
        else
        {
            Debug.Log("Hit Location Bug");
        }
    }
    public void AddDestination(Vector3 direction)
    {
        if (direction == Vector3.up)
            currentDirection = up;
        else if (direction == Vector3.down)
            currentDirection = down;
        else if (direction == Vector3.right)
            currentDirection = right;
        else if (direction == Vector3.left)
            currentDirection = left;
        else
            return;
        destination += direction * scale;
        speed = oldSpeed;
        canMove = false;
    }
}
