﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Image),typeof(RectTransform))]
public class BeatIcon : MonoBehaviour
{
    SongManager m_SongManager = null;
    Image m_Image = null;
    RectTransform m_Transform = null;

    private Vector3 m_StartScale = Vector3.one;
    private Vector3 m_LargeScale = Vector3.one;
    // Start is called before the first frame update
    void Start()
    {
        m_SongManager = FindObjectOfType<SongManager>();
        m_Image = GetComponent<Image>();
        m_Transform= GetComponent<RectTransform>();

        m_StartScale = m_Transform.localScale;
        m_LargeScale = m_StartScale * 1.15f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var inBeat =m_SongManager.IsInBeatTime();

        if(inBeat){
            // m_Image.color = Color.green;
            m_Transform.localScale = m_LargeScale;
        }else{
            // m_Image.color = Color.white;
            m_Transform.localScale = m_StartScale;
        }
    }
}
