﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    [SerializeField] Vector3 rotateTarget= Vector3.zero;
    [SerializeField] float rotateSpeed = 30f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(rotateTarget, Vector3.up, rotateSpeed * Time.deltaTime);
    }
}
