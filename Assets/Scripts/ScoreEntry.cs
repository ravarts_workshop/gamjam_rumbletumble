﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreEntry : MonoBehaviour
{

    [SerializeField] Text m_MissNotes = null;
    [SerializeField] Text m_HitNotes = null;
    [SerializeField] Text m_TotalNotes = null;
    [SerializeField] Image m_Crown = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetScores(string missNotes, string HitNotes, string TotalNotes){
        m_MissNotes.text = missNotes;
        m_HitNotes.text = HitNotes;
        m_TotalNotes.text = TotalNotes;
    }
    public void HighestScore()
    {
        m_Crown.gameObject.SetActive(true);
    }
}
