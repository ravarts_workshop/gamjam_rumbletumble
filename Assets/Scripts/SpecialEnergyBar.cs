﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialEnergyBar : MonoBehaviour
{

    [SerializeField] Image m_EnergyBar = null;
    Text m_Text = null;
    [SerializeField] Animator maskAnimator = null;


    // Start is called before the first frame update
    void OnEnable()
    {
        m_Text = GetComponentInChildren<Text>();

    }

    internal void ResetEnergy()
    {
        if (!m_EnergyBar) { return; }

        m_EnergyBar.fillAmount = 0f;
    }



    internal void setPlayerNumber(string playerNumber)
    {
        m_Text.text = playerNumber;
    }

    internal void UpdateEnergyBar(float value)
    {
        m_EnergyBar.fillAmount = value;
    }

    internal void SetColor(Color color)
    {
        m_EnergyBar.color = color;
    }

    internal void EnableSpecial(bool toEnable)
    {

        if (!maskAnimator) { return; }
        maskAnimator.SetBool("EnableAnimation", toEnable);

    }


}
