﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 
/// </summary>
public class SongManager : MonoBehaviour
{

    [SerializeField] private float m_AdjustmentInS = 0f; //#TODO: Figure out 
    [SerializeField] private float m_ReduceSongLengthInS = 0f;
    // 4/4 time = 4, 3/4 time = 3, 3/8 time = 3 etc.
    // [SerializeField] private int m_NumBeatsPerSegment = 4;


    // beats per minute
    [SerializeField] private float m_Bpm = 120f;

    //the current position of the song (in seconds)
    private float m_SongPosition;

    //the current position of the song (in beats)
    private float m_SongPosInBeats;

    public float SecPerBeat { get; private set; }
    public float SongPosInBeats { get => m_SongPosInBeats; private set => m_SongPosInBeats = value; }
    public float TimingThreshold { get => m_TimingThreshold; private set => m_TimingThreshold = value; }

    //how much time (in seconds) has passed since the song started
    private float m_Dsptimesong;

    // leniency of timing a beat +- of beat
    private float m_TimingThreshold = 0.1f;

    private float m_BeatToEndOn = 0f;
    private float nextBeatTime = 0f;

    private AudioSource m_AudioSource = null;
    private GameManager m_GameManager = null;

    public delegate void OnBeat(int beat);
    public static event OnBeat onBeat;

    public delegate void AfterBeat(int beat);
    public static event AfterBeat afterBeat;

    public delegate void SongEnded();
    public static event SongEnded songEnded;
    private void OnEnable()
    {
        //calculate how many seconds is one beat
        SecPerBeat = 60f / m_Bpm;

        //record the time when the song starts
        m_Dsptimesong = (float)AudioSettings.dspTime + m_AdjustmentInS;

        //start the song
        m_AudioSource = GetComponent<AudioSource>();
        m_AudioSource.Play();

        //record the beat when the song is supposed to end
        m_BeatToEndOn = ((m_AudioSource.clip.length - m_ReduceSongLengthInS) / SecPerBeat) - 4;
        m_GameManager = FindObjectOfType<GameManager>();
    }

    /// <summary>
    /// 
    /// </summary>
    private void FixedUpdate() //#TODO update vs FixedUpdate for Beats accuracy
    {
        if (!m_GameManager.GameStarted) { return; }

        int beat = Mathf.RoundToInt(SongPosInBeats);
        if (beat >= m_BeatToEndOn)
        {
            RaiseEndSong();
        }
        bool inBeatTime = IsInBeatTime();
        bool nextBeatPossible = (nextBeatTime - SongPosInBeats) < 0f;
        if (inBeatTime && nextBeatPossible)
        {
            onBeat(beat); //beat event
            //set next earliest beat time
            nextBeatTime = (SongPosInBeats + 1) - TimingThreshold;
            var afterBeatDelay = SongPosInBeats % 1;
            if (afterBeatDelay > TimingThreshold)
            {//~0.9f+
                afterBeatDelay -= 1;//Distance to 1
            }
            afterBeatDelay += TimingThreshold;
            StartCoroutine(RaiseAfterBeat(afterBeatDelay, beat));
        }

    }

    private void RaiseEndSong()
    {
        songEnded();
    }

    private IEnumerator RaiseAfterBeat(float timingThreshold, int beat)
    {
        yield return new WaitForSeconds(timingThreshold);

        afterBeat(beat);
    }

    private void UpdateBeat()
    {
        //calculate the position in seconds
        m_SongPosition = (float)(AudioSettings.dspTime - m_Dsptimesong);

        //calculate the position in beats
        SongPosInBeats = m_SongPosition / SecPerBeat;
    }

    /// <summary>
    /// Calculates if current time in the song is in the beat, returns true if that's the case.
    /// </summary>
    /// <returns>bool if in beat</returns>
    internal bool IsInBeatTime()
    {
        if (!m_AudioSource.isPlaying) { return false; }

        UpdateBeat();
        float timeOfBeat = SongPosInBeats % 1;
        bool inBeatTime = (timeOfBeat > (1 - TimingThreshold)
                || timeOfBeat < TimingThreshold);
        return inBeatTime;
    }
    /// <summary>
    /// Calculates if current time in the song is in the beat, returns true if that's the case.
    /// 
    /// </summary>
    /// <param name="timingThreshold">threshold to accept</param>
    /// <returns>bool if in beat</returns>
    internal bool IsInBeatTime(float timingThreshold)
    {
        if (!m_AudioSource.isPlaying) { return false; }

        UpdateBeat();
        var Threshold = Mathf.Clamp(timingThreshold, 0f, 1f);
        float timeOfBeat = SongPosInBeats % 1;
        bool inBeatTime = (timeOfBeat > (1 - Threshold)
                || timeOfBeat < Threshold);
        return inBeatTime;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns>distance in float 0f->0.1f to last beat in beat units</returns>
    internal float OffBeatTime()
    {

        UpdateBeat();
        float timeOfBeat = SongPosInBeats % 1;
        return timeOfBeat; //
    }
}
