﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Player))]
public class SpecialEnergy : MonoBehaviour
{
    public float CurrentEnergy { get; private set; } = 0f;

    public float MaxEnergy { get; private set; } = 100f;

    private float m_EnergyRegen = 3f; //per second
    private float m_EnergyConsumptionRate = 10f; //per second

    [SerializeField] private SpecialEnergyBar m_EnergyBar = null;
    private Player m_Player;
    private GameManager m_GameManager;
    public bool SpecialActivated { get; private set; } = false;
    // Start is called before the first frame update
    void Start()
    {
        //Components
        m_Player = GetComponent<Player>();
        var inputControls = GetComponent<InputController>();
        m_GameManager = FindObjectOfType<GameManager>();


        //setup Energy Bar
        if (m_EnergyBar && inputControls)
        {
            m_EnergyBar.setPlayerNumber(inputControls.getInputPlayer().ToString());
            m_EnergyBar.SetColor(m_Player.Color);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    internal void ResetEnergy()
    {
        CurrentEnergy = 0f;
        if (m_EnergyBar)
        {
            m_EnergyBar.ResetEnergy();
        }
    }
    /// <summary>
    /// Clamps Energy Between 0 and MaxEnergy
    /// </summary>
    /// <param name="value"></param>
    internal void AddEnergy(float value)
    {
        CurrentEnergy = Mathf.Clamp(CurrentEnergy + value, 0, MaxEnergy);

    }

    // Update is called once per frame
    void Update()
    {
        if (!m_GameManager.GameStarted) { return; }
        UpdateEnergy();

        if (!m_EnergyBar) { return; }

        m_EnergyBar.UpdateEnergyBar(CurrentEnergy / MaxEnergy);

    }

    private void UpdateEnergy()
    {
        if (SpecialActivated)
        {
            AddEnergy(-m_EnergyConsumptionRate * Time.deltaTime);
        }
        else
        {
            AddEnergy(m_EnergyRegen * Time.deltaTime);
        }
        if (CurrentEnergy >= MaxEnergy)
        {
            SpecialActivated = true;

            if (!m_EnergyBar) { return; }
            m_EnergyBar.EnableSpecial(true);
        }
        else if (Mathf.Approximately(CurrentEnergy, 0))
        {
            SpecialActivated = false;
            m_EnergyBar.EnableSpecial(false);
        }
    }
}
