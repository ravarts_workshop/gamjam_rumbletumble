﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(PlayerMovement))]
public class Player : MonoBehaviour
{
    [SerializeField] private Color m_Color = Color.white;
    [SerializeField] private SoundToPlay m_HitSound = null; 
    [SerializeField] private SoundToPlay m_StepSound = null;
    [SerializeField] private SoundToPlay m_RopeSound = null;
    [SerializeField] private GameObject m_MusicNote = null;
    [SerializeField] private Transform m_MusicNoteSpawn = null;

    private PlayerMovement m_PlayerMovement = null;
    private SpecialEnergy m_Energy = null;
    public float NextPossibleBeatTime { get; set; } = 0f;

    public int NextPossibleBeat { get; set; } = 0;
    public Color Color { get => m_Color; }

    public int Score { get; private set; } = 0;
    public int MissedNotes { get; private set; } = 0;


    private SoundPlayer m_SoundPlayer = null;

    private SongManager m_SongManager = null;


    // Start is called before the first frame update
    void Start()
    {
        m_PlayerMovement = GetComponent<PlayerMovement>();
        m_SoundPlayer = FindObjectOfType<SoundPlayer>();
        m_Energy = GetComponent<SpecialEnergy>();
        m_SongManager = FindObjectOfType<SongManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void HandleInput(PlayerAction action)
    {
        m_PlayerMovement.ExecuteMoveInput(action); //#TODO only call with move actions
    }

    public void IncreaseScore()
    {
        Score++;
    }

    internal void RegisterHit()
    {
        if (SpecialActivated() && m_SongManager)
        {
            StartCoroutine(QueueAnotherHit(m_SongManager.SecPerBeat / 2));
        }
        IncreaseScore();
        AddEnergy();
        PlaySound(m_HitSound);
        SpawnMusicNote();
    }

    private void SpawnMusicNote()
    {
        if (!m_MusicNote) { return; }
        var musicNote = Instantiate(m_MusicNote, m_MusicNoteSpawn.position, Quaternion.identity);
        var spriteRenderer = musicNote.GetComponent<SpriteRenderer>();
        spriteRenderer.color = m_Color;
    }

    private void AddEnergy()
    {
        if (!m_Energy) { return; }
        m_Energy.AddEnergy(5f);
    }
    /// <summary>
    /// 
    /// </summary>
    private void PlaySound(SoundToPlay soundToPlay)
    {
        //Play Sound
        if (soundToPlay !=null && m_SoundPlayer)
        {
            m_SoundPlayer.PlaySound(soundToPlay.soundFile,m_HitSound.volume);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="hitSound"></param>
    /// <param name="delay"></param>
    /// <returns></returns>
    private IEnumerator QueueAnotherHit( float delay)
    {
        yield return new WaitForSeconds(delay);

        IncreaseScore();
        PlaySound(m_HitSound);
        SpawnMusicNote();
    }
    /// <summary>
    /// 
    /// </summary>
    internal void AddMiss()
    {
        MissedNotes++;
    }

    public bool SpecialActivated()
    {
        if (!m_Energy) { return false; }

        return m_Energy.SpecialActivated;
    }

    internal void PlayStepSound()
    {
        PlaySound(m_StepSound);
    }

    internal void PlayRopeSound()
    {
        PlaySound(m_RopeSound);
    }
}
