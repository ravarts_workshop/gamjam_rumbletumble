﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialHint : MonoBehaviour
{
    Camera MainCamera =null;
    // Start is called before the first frame update

    void Start()
    {
        MainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = MainCamera.transform.rotation;
    }
}
