﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class BeatSlider : MonoBehaviour
{
    [SerializeField] Image m_BeatSlider =null;
    private float m_MovementSpeed = 466.66f;


    // Start is called before the first frame update
    RectTransform m_Transform = null;
    private Vector2 m_TravelDir = Vector2.right;
    public float MovementSpeed { get => m_MovementSpeed; set => m_MovementSpeed = value; }
    public Vector2 TravelDir { get => m_TravelDir; set => m_TravelDir = value; }

    void Start()
    {
        m_Transform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {

        var newPos = new Vector2(m_Transform.anchoredPosition.x + TravelDir.x * Time.deltaTime * MovementSpeed,
                                m_Transform.anchoredPosition.y + TravelDir.y);

        m_Transform.anchoredPosition = newPos;
        //Destroy when reached beat
        if (TravelDir == Vector2.left && m_Transform.anchoredPosition.x < 0)
        {
            Destroy(gameObject);
        }
        else if (TravelDir == Vector2.right && m_Transform.anchoredPosition.x > 0)
        {
            Destroy(gameObject);
        }
    }

    public void setColor(Color color)
    {
        if(!m_BeatSlider){ return; }
        m_BeatSlider.color = color;

    }
}
