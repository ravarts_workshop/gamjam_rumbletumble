﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CharacterInputs
{

    public CharacterInputs(KeyCode up, KeyCode down, KeyCode left, KeyCode right, KeyCode special)
    {
        this.Up = up;
        this.Down = down;
        this.Left = left;
        this.Right = right;
        this.Special = special;
    }

    //Input Controls
    internal KeyCode Up;
    internal KeyCode Down;
    internal KeyCode Left;
    internal KeyCode Right;
    internal KeyCode Special;
}
public enum InputPlayer
{
    P1, P2, P3, P4
}
public class InputUtils : MonoBehaviour
{
    static public CharacterInputs CreateInputs(InputPlayer player)
    {
        CharacterInputs inputs;
        switch (player)
        {
            case (InputPlayer.P1):
            {
                inputs = new CharacterInputs(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D, KeyCode.Space);
                break;
            }
            case (InputPlayer.P2):
            {
                inputs = new CharacterInputs(KeyCode.I, KeyCode.K, KeyCode.J, KeyCode.L, KeyCode.H);
                break;
            }
            case (InputPlayer.P3):
            {
                inputs = new CharacterInputs(KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.Return);
                break;
            }
            case (InputPlayer.P4):
            {
                inputs = new CharacterInputs(KeyCode.Keypad8, KeyCode.Keypad5, KeyCode.Keypad4, KeyCode.Keypad6, KeyCode.Keypad0);
                break;
            }
            default:
            {
                inputs = new CharacterInputs();
                break;
            }
        }
        return inputs;
    }
}
