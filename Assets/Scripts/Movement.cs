﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 5f;
    //for Jump or other moves
    private float oldSpeed;
    public float scale = 1f;
    float rayLength = 1f;
    bool canMove;

    //Rotation
    Vector3 up = Vector3.zero;
    Vector3 right = new Vector3(0, 90, 0);
    Vector3 down = new Vector3(0, 180, 0);
    Vector3 left = new Vector3(0, 270, 0);
    Vector3 currentDirection = Vector3.zero;

    Vector3 nextPos, destination, direction;

    void Start()
    {
        currentDirection = up;
        nextPos = Vector3.forward;
        destination = transform.position;
        oldSpeed = speed;
    }
    
    void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);


        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            nextPos = Vector3.forward * scale;
            currentDirection = up;
            speed = oldSpeed;
            canMove = true;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            nextPos = Vector3.back * scale;
            currentDirection = down;
            speed = oldSpeed;
            canMove = true;
        }
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            nextPos = Vector3.right * scale;
            currentDirection = right;
            speed = oldSpeed;
            canMove = true;
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            nextPos = Vector3.left * scale;
            currentDirection = left;
            speed = oldSpeed;
            canMove = true;
        }

        //completed one step
        if (Vector3.Distance(destination, transform.position) <= 0.001f)
        {
            transform.localEulerAngles = currentDirection;
            if (canMove)
            {
                //For Walls
                if (Valid())
                {
                    destination = transform.position + nextPos;
                    direction = nextPos;
                    canMove = false;
                }
                //JumpMove with Ropes
                if (CheckJump())
                {
                    //Trigger Animation
                    destination = transform.position - nextPos * 3;
                    direction = -nextPos;
                    oldSpeed = speed;
                    speed *= 3;
                    canMove = false;
                    //ToDo Skip one turn
                    //Do Collision Player DMG/Sound
                }
            }
        }

        bool Valid()
        {
            Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
            RaycastHit hit;

            Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

            if(Physics.Raycast(myRay,out hit, rayLength * scale))
            {
                if (hit.collider.CompareTag("Wall"))
                {
                    return false;
                }
            }
            return true;
        }
        bool CheckJump()
        {
            Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0), transform.forward);
            RaycastHit hit;

            Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

            if (Physics.Raycast(myRay, out hit, rayLength))
            {
                if (hit.collider.CompareTag("Rope"))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
