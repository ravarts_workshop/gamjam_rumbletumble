﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum PlayerAction
{
    Up, Down, Left, Right, Special, BackToMenu
}

public class GameManager : MonoBehaviour
{
    [SerializeField] [Tooltip("In Beats")] float m_RequiredBeatAccuracy = 0.3f; //1 = 1beat (e.g. 0.5s for 120bpm)
    [SerializeField] Player[] m_Players = null;
    [SerializeField] bool deActivateRoundRobin = false;
    [SerializeField] Animator m_CountdownAnimator = null;
    [SerializeField] ScoreBoard m_Scoreboard = null;
    private SongManager m_SongManager = null;

    public bool GameStarted { get; private set; } = false;
    public bool GameEnded { get; private set; } = false;
    //player/ input mapping
    Dictionary<InputPlayer, Player> m_PlayerDB = new Dictionary<InputPlayer, Player>();


    private void OnEnable()
    {
        //subscribe to Events
        SongManager.onBeat += OnBeat;
        SongManager.afterBeat += AfterBeat;
        SongManager.songEnded += EndGame;
    }
    private void OnDisable()
    {
        //unsubscribe from Events
        SongManager.onBeat -= OnBeat;
        SongManager.afterBeat -= AfterBeat;
        SongManager.songEnded -= EndGame;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="beat"></param>
    private void AfterBeat(int beat)
    {
        // Debug.Log("Afterbeat: " + beat);
        var playerBeat = m_Players[(beat % m_Players.Length)].NextPossibleBeat;
        if (playerBeat < beat)
        {
            if (deActivateRoundRobin)
            {

            }
            else
            {
                m_Players[(beat % m_Players.Length)].NextPossibleBeat += m_Players.Length;
                //add miss?
                // m_Players[(beat % m_Players.Length)].AddMiss();
            }
        }
    }
    private void OnBeat(int beat)
    {

    }
    // Start is called before the first frame update
    void Start()
    {
        LoadGameSettings();
        m_SongManager = FindObjectOfType<SongManager>();

        StartGame();
        SetupFirstBeatTimes();

    }

    private void LoadGameSettings()
    {
        var settings = FindObjectOfType<GameSettings>();
        if (!settings) { return; }

        //No of Players
        var noPlayers = settings.AmountOfPlayers;
        bool isPlayer = true;
        for (int i = 0; i < m_Players.Length; i++)
        {
            if (noPlayers <= 0) { isPlayer = false; }
            var inputControls = m_Players[i].GetComponent<InputController>();
            if (inputControls)
            {
                inputControls.IsAI = !isPlayer;
            }
            noPlayers--;
        }
    }

    private void SetupFirstBeatTimes()
    {
        //Setup first beat times
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_PlayerDB.Add((InputPlayer)i, m_Players[i]);
            m_Players[i].NextPossibleBeatTime = i + 1;
            m_Players[i].NextPossibleBeat = i + m_Players.Length;
        }
    }


    /// <summary>
    /// Receives and Handles Input sent from InputController
    /// </summary>
    /// <param name="inputPlayer"></param>
    /// <param name="action"></param>
    internal void PlayerInput(InputPlayer inputPlayer, PlayerAction action)
    {
        if (GameEnded && action == PlayerAction.BackToMenu)
        {
            SceneManager.LoadScene(0);
        }

        if (!m_SongManager || !GameStarted) { return; }
        Player activePlayer;
        m_PlayerDB.TryGetValue(inputPlayer, out activePlayer);
        if (!activePlayer) { return; }

        bool isInBeat = m_SongManager.IsInBeatTime(m_RequiredBeatAccuracy);
        //#TODO:Special Meter perfect offbeat check
        //bool is8thBeat = m_SongManager.is8thBeatTime(m_RequiredBeatAccuracy);

        //player in beat and players turn
        if (isInBeat && (activePlayer.NextPossibleBeat == Mathf.RoundToInt(m_SongManager.SongPosInBeats) || deActivateRoundRobin))
        {
            activePlayer.HandleInput(action);
            if (deActivateRoundRobin)
            {
                activePlayer.NextPossibleBeat += 1;
            }
            else
            {
                activePlayer.NextPossibleBeat += m_Players.Length;
            }
        }
        else
        {
            //Debug.Log("Input missed at: " + m_SongManager.SongPosInBeats);
            //play wrong note (substract score?)
            activePlayer.AddMiss();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Player getCurrentPlayer()
    {
        if (!m_SongManager && m_Players.Length <= 1) { return null; }
        var playerIndex = Mathf.RoundToInt(m_SongManager.SongPosInBeats) % m_Players.Length;
        //player adjustment
        if (playerIndex == 0) { playerIndex = m_Players.Length - 1; }
        else { playerIndex--; }

        var player = m_Players[playerIndex];
        return player;
    }
    /// <summary>
    /// 
    /// </summary>
    internal void EndGame()
    {
        GameStarted = false;
        GameEnded = true; //#TODO: Use States instead
        //Load Scoreboard Scene with Score

        m_Scoreboard.AddScores(m_Players);
        m_Scoreboard.gameObject.SetActive(true);
    }
    /// <summary>
    /// 
    /// </summary>
    private void StartGame()
    {
        //Start Countdown
        StartCoroutine(StartGameRoutine());
        //Start Game
        GameStarted = true;

    }
    public IEnumerator StartGameRoutine()
    {
        //Countdown 3,2,1,Go
        if (!m_CountdownAnimator) { GameStarted = true; yield return 0; }
        for (int i = 0; i < 4; i++)
        {
            m_CountdownAnimator.SetTrigger("SpawnNext");

            yield return new WaitForSeconds(m_SongManager.SecPerBeat);
        }
        m_CountdownAnimator.gameObject.SetActive(false);
    }
}
