﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStartScreen : MonoBehaviour
{
    Animator anim;
    float random;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        random = (int)Random.Range(0, 10);
    }

    // Update is called once per frame
    void Update()
    {
       if(random % 30 > 15)
        {
            anim.SetBool("Dance", true);
        }
        else if( random % 30 < 10)
        {
            anim.SetBool("Dance", false);
        }
        random += 1 * Time.deltaTime;
        
    }
}
