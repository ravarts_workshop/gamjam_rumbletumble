﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatSliderSpawner : MonoBehaviour
{
    [SerializeField] GameObject m_SliderPrefab = null;
    [SerializeField] float m_DistanceBetweenSliders = 300f;
    [SerializeField] int m_BeatsToDisplayInAdvance = 4;
    [SerializeField] Vector3 m_SpawnSize = Vector3.one;
    float m_MovementSpeed = 400f;

    SongManager m_SongManager = null;
    private GameManager m_GameManager = null;


    bool firstBeatPlayed = false;
    // Start is called before the first frame update

    private void OnEnable()
    {
        SongManager.onBeat += OnBeat;
        SongManager.songEnded += SongEndedEvent;
    }
    private void OnDisable()
    {
        SongManager.onBeat -= OnBeat;
        SongManager.songEnded -= SongEndedEvent;
    }
    private void SongEndedEvent()
    {
        //Destroy all remaining Sliders
        var sliders = FindObjectsOfType<BeatSlider>();
        foreach (BeatSlider slider in sliders)
        {
            Destroy(slider.gameObject);
        }
    }

    void Start()
    {
        m_SongManager = FindObjectOfType<SongManager>();
        m_GameManager = FindObjectOfType<GameManager>();
        if (m_SongManager)
        {
            //calculate movementSpeed
            m_MovementSpeed = m_DistanceBetweenSliders / m_SongManager.SecPerBeat;
        }

    }
    private void OnBeat(int beat)
    {
        //#TODO: Stop Spawning near end
        InstantiateSliders(m_BeatsToDisplayInAdvance);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stepDistance"></param>
    private void InstantiateSliders(int stepDistance)
    {
        //left side
        var sliderPosL = new Vector3(transform.position.x - m_DistanceBetweenSliders * stepDistance, transform.position.y, transform.position.z);
        var sliderL = Instantiate(m_SliderPrefab, sliderPosL, Quaternion.identity, transform);
        sliderL.transform.localScale = m_SpawnSize;
        var beatSliderL = sliderL.GetComponent<BeatSlider>();
        beatSliderL.TravelDir = Vector2.right;
        beatSliderL.MovementSpeed = m_MovementSpeed;
        beatSliderL.setColor(m_GameManager.getCurrentPlayer().Color);

        //right side
        var sliderPosR = new Vector3(transform.position.x + m_DistanceBetweenSliders * stepDistance, transform.position.y, transform.position.z);
        var sliderR = Instantiate(m_SliderPrefab, sliderPosR, Quaternion.identity, transform);
        sliderR.transform.localScale = m_SpawnSize;
        var beatSliderR = sliderR.GetComponent<BeatSlider>();
        beatSliderR.TravelDir = Vector2.left;
        beatSliderR.MovementSpeed = m_MovementSpeed;
        beatSliderR.setColor(m_GameManager.getCurrentPlayer().Color);


    }
    /// <summary>
    /// Only Spawns First set of sliders on the very first beat 
    /// </summary>
    /// <param name="inBeat"></param>
    private void SpawnFirstSetOfSliders(bool inBeat)
    {
        if (!firstBeatPlayed && inBeat)
        {
            for (int stepDistance = 1; stepDistance < m_BeatsToDisplayInAdvance + 1; stepDistance++)
            {
                InstantiateSliders(stepDistance);
            }
            firstBeatPlayed = true;
        }
    }
}
