﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public struct SelectableSong
{
    public SelectableSong(int bpm, string songName, float beatAdjustment)
    {
        this.Bpm = bpm;
        this.SongName = songName;
        this.BeatAdjustment = beatAdjustment;
    }
    int Bpm;
    string SongName;
    float BeatAdjustment;
}
/// <summary>
/// 
/// </summary>
public class GameSettings : MonoBehaviour
{
    public static GameSettings init;

    public int AmountOfPlayers { get; set; } = 2;

    public float Volume { get; set; } = 0.1f;

    public SelectableSong selectedSong { get; set; } = new SelectableSong(0, "", 0);
    public static GameSettings Instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
    //Singleton
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
