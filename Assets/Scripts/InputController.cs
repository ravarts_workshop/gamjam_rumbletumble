﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    [SerializeField] [Tooltip("Player slot")] private InputPlayer m_InputPlayer = InputPlayer.P1;
    [SerializeField] private bool isAI = false;
    public InputPlayer getInputPlayer()
    {
        return m_InputPlayer;
    }
    private CharacterInputs m_Inputs;


    private GameManager m_GameManager = null;

    public bool IsAI { get => isAI; set => isAI = value; }
    private void OnEnable()
    {
        SongManager.onBeat += RandomPlayerAction;
        
    }
    private void OnDisable()
    {
        SongManager.onBeat -= RandomPlayerAction;
    }
    void Start()
    {
        //setup 
        m_Inputs = InputUtils.CreateInputs(m_InputPlayer);
        m_GameManager = FindObjectOfType<GameManager>();
    }
    /// <summary>
    /// 
    /// </summary>
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(m_Inputs.Up))
        {
            m_GameManager.PlayerInput(m_InputPlayer, PlayerAction.Up);
        }
        else if (Input.GetKeyDown(m_Inputs.Down))
        {
            m_GameManager.PlayerInput(m_InputPlayer, PlayerAction.Down);
        }
        else if (Input.GetKeyDown(m_Inputs.Left))
        {
            m_GameManager.PlayerInput(m_InputPlayer, PlayerAction.Left);
        }
        else if (Input.GetKeyDown(m_Inputs.Right))
        {
            m_GameManager.PlayerInput(m_InputPlayer, PlayerAction.Right);
        }
        else if (Input.GetKeyDown(m_Inputs.Special))
        {
            m_GameManager.PlayerInput(m_InputPlayer, PlayerAction.Special);
        }else if(Input.GetKeyDown(KeyCode.Escape)){
            m_GameManager.PlayerInput(m_InputPlayer, PlayerAction.BackToMenu);
        }

    }
    /// <summary>
    /// Sends Random Action to GameManager
    /// </summary>
    /// <param name="beat"></param>
    private void RandomPlayerAction(int beat)
    {
        if (!IsAI) { return; }
        PlayerAction[] actions = { PlayerAction.Up, PlayerAction.Down, PlayerAction.Left, PlayerAction.Right };
        PlayerAction action = actions[UnityEngine.Random.Range(0, actions.Length)];

        m_GameManager.PlayerInput(m_InputPlayer, action);
    }
}
