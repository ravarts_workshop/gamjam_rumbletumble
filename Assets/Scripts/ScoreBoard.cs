﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
    [SerializeField] ScoreEntry[] m_PlayerScores;

    private int highscore = 0;
    ScoreEntry highest = null;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void AddScores(Player[] m_Players)
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            //Create ScoreEntry
            var missNotes = m_Players[i].MissedNotes;
            var PlayerInput = m_Players[i].GetComponent<InputController>();
            if (PlayerInput && PlayerInput.IsAI)
            {
                missNotes = Mathf.RoundToInt((missNotes / 4) * UnityEngine.Random.Range(0.2f, 1.0f));
            }
            var hitNotes = m_Players[i].Score;
            if (m_PlayerScores.Length >= i + 1)
            {
                var hitScore = hitNotes * 1500;
                var missScore = missNotes * 100;
                var score = Mathf.Clamp((hitScore - missScore), 0, float.MaxValue);
                m_PlayerScores[i].SetScores(missNotes.ToString(), hitNotes.ToString(), score.ToString());

                if ((int)score > highscore)
                {
                    highscore = (int)score;
                    highest = m_PlayerScores[i];
                }
            }


        }
        if (highest)
        {
            highest.HighestScore();
        }
    }
}
