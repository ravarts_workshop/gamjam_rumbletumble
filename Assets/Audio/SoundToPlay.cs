﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Audio/SoundEffect")]
public class SoundToPlay : ScriptableObject
{
    public AudioClip soundFile = null;
    public float volume = 1f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
